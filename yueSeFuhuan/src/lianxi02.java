public class lianxi02 {
    public int[] searchRange (int[] nums, int target) {
        // write code here
        int[] array = {-1,-1};
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] ==target){
                array[0] = i;
                break;
            }
            if (array[0] ==-1){
                return array;
            }
        }
        for (int j = nums.length-1; j >= 0 ; j--) {
            if (nums[j] == target){
                array[1] = j;
                break;
            }
        }
        return array;
    }

}
