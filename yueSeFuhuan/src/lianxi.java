import java.util.Arrays;

public class lianxi {

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param skuAmount string字符串一维数组
     * @return string字符串
     */
    public static String calcPayAmount (String[] skuAmount) {
        // write code here
        int i = 0;
        double[] c=  new double[skuAmount.length];
        for (String a:skuAmount) {
            Double b = Double.valueOf(a);
            c[i] = b;
            i++;
        }
        Arrays.sort(c);
        double sum = 0;
        if (c.length %2 ==0){
            for (int j = 1; j < c.length; j+=2) {
                sum +=c[j];
            }
        }else {
                for (int j = c.length-1; j > 0; j-=2) {
                    sum= c[j];
            }

            //return String.valueOf(sum+c[0]);
            return String.format("%.2f", sum);
        }
        return String.format("%.2f", sum);
    }

    public static void main(String[] args) {
        String[] skuAmount = new String[]{"199.90","39.90","29.90","59.90"};
        String[] skuAmount1 = new String[]{"1","2","3","4","5"};
        System.out.println(calcPayAmount(skuAmount1));
        byte b = 2;
        System.out.println("123456");
        System.out.println("666666666666666");
    }

}
