package com.cqh.JZOffer;

import java.util.*;

public class JZ3 {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param numbers int整型一维数组
     * @return int整型
     */
    public int duplicate (int[] numbers) {
        // write code here
        for (int i = 0; i < numbers.length-1; ++i){
            for (int j = i+1; j < numbers.length; ++j) {
                if (numbers[i] == numbers[j]){
                    return numbers[i];
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数组:");
        String str = sc.nextLine();
        String[] Arrays = str.split(",");  //通过“,”分离
        int[] a = new int[Arrays.length];
        for (int i = 0; i < a.length; i++) {
            a[i] = Integer.parseInt(Arrays[i]);
        }
        for (int j = 0; j < a.length; j++) {
            System.out.print(a[j]);
        }
        System.out.println();

        JZ3 jz3 = new JZ3();
        int b = jz3.duplicate(a);
        System.out.println(b);
    }
}


