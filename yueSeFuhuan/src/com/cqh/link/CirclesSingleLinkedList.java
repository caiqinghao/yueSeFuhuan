package com.cqh.link;


/**
 *   环形链表
 */
public class CirclesSingleLinkedList {

    //初始化一个头节点,不指向任何结点
    Boy first = new Boy(-1);

    /**
     * 构建环形链表
     */
    public void addBoy(int nums){
        //判断输入的结点数是否合法
        if (nums < 1){
            System.out.println("输入的结点数不合法");
            return;
        }
        //中间结点
        Boy temp = null;
        //开始构建环
        for (int i = 1; i <= nums; i++) {
            //每一次循环生成一个结点
            Boy boy = new Boy(i);
            if (i == 1){  // 第一个结点
                first = boy;  //头节点就是它本身
                boy.setNext(first); //自己指向自己，形成环
                temp = first;   //更新中间结点
            }else {  //如果不是自有一个结点
                temp.setNext(boy);   //中间结点指向 它
                boy.setNext(first);  //下一个结点指向头结点，形成环
                temp = boy;          //更新中间结点
            }
        }
    }

    /**
     *  遍历结点
     */
    public void showBoy(){
        //如果头结点为空
        if (first == null){
            System.out.println("环形链表为空");
            return;
        }

        //头结点不为空
        Boy boy = first;
        while (true){
            System.out.print("输出的编号为："+boy.getNo());
            if (boy.getNext()==first){
                break;
            }
            boy = boy.getNext();
        }
    }

    /**
     *  输入第几个开始数，数多少个，环形中一共有多少个
     */
    public void countBoy(int startNo,int countNum,int nums){

        if (first == null || startNo < 1 || countNum > nums){
            System.out.println("输入参数错误");
        }

        Boy helper = first;
        //找到最后一个结点
        while (true){
            if (helper.getNext() == first){
                break;
            }
            helper = helper.getNext();
        }

        //输入数多少个，找到first
        for (int i = 0; i < startNo-1; i++) {
            if (first == helper){
                break;
            }
            first = first.getNext();
            helper = helper.getNext();
        }

        System.out.println("\n");
        //出列
        while (true){
            if (first == helper){
                break;
            }
            for (int i = 0; i < countNum-1; i++) {
                first = first.getNext();
                helper = helper.getNext();
            }
            System.out.println("出列："+first.getNo());
            first = first.getNext();
            helper.setNext(first);
        }
        System.out.println("最后出列的："+first.getNo());
    }

    public static void main(String[] args) {
        CirclesSingleLinkedList c = new CirclesSingleLinkedList();
        c.addBoy(5);
        c.showBoy();
        c.countBoy(1, 3, 5);
    }

}
