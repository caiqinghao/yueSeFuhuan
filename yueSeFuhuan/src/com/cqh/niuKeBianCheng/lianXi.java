package com.cqh.niuKeBianCheng;
import java.util.Scanner;

public class lianXi {
    public static void main(String[] args) {
        /*Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int ans = 0;
        // 最后一轮剩下2个人，所以从2开始反推
        for (int i = 2; i <= a; i++) {
            ans = (ans + 3) % i;
        }
        System.out.println(ans+1);*/


        /**
         * 数组约瑟夫环
         */
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] p = new int[n];        //定义一个数组
        int[] flag = new int[n];     //定义一个标记数组，点到名标记 1 ，视为出数组。为点到标记 0
        for (int i = 0; i < n; i++) {
            p[i] = i + 1;       //初始化数组
            flag[i] = 0;        //初始默认都在圈里
        }

        int c = 0;  //当前喊的人的号牌
        int k = 0;  //当前喊的数
        int targetNum = 3;  //喊到 3 的出数组，标记为1
        int outNums = 0;    //已经出数组的人数
        //当出队人数不等于 输入的人数时，就继续数
        while (outNums != n-1){
            c++;    //1号位准备喊数
            if (c > n){  //如果当前喊的人的位置，大于总数，则重新开始喊
                c = 1;
            }
            //如果这个人在圈里
            if (flag[c-1] == 0){
                k++;  //让他喊数
                if (k == targetNum){   //如果喊的数等于目标值。则标记
                    flag[c-1] = 1;     //标记已经出圈，不再参与下一轮
                    outNums++;         //出圈人数加1
                    k = 0;             //更新喊的数，重新开始喊
                }
            }
        }
        for (int i = 0; i < flag.length; i++) {
            if (flag[i] == 0){
                System.out.println(p[i]);
            }
        }
    }
}