package com.cqh.niuKeBianCheng;

public class Time {
    int Date(int y,int m,int d)
    {
        if(m==1||m==2){//一二月换算
            m+=12;
            y--;
        }
        int week = (d + 2*m +3*(m+1)/5 + y + y/4 - y/100 + y/400 + 1)%7;
        return week;//其中1~7表示周一到周日
    }

    public static void main(String[] args) {
        Time time = new Time();
        System.out.println(time.Date(1970, 1, 2)==time.Date(2020, 2, 7));
    }
}
