package com.cqh.maze;


/**
 * 递归---迷宫
 */
public class MazeApp {

    public static void main(String[] args) {

        //定义迷宫的整体为一个二维数组
        int[][] map = new int[8][7];
        //有颜色的设置为 1 ，没有颜色的设置为 0
        for (int i = 0; i < 7; i++) {
            map[0][i] = 1;
            map[7][i] = 1;
        }
        for (int i = 0; i < 8; i++) {
            map[i][0] = 1;
            map[i][6] = 1;
        }
        map[3][1] = 1;
        map[3][2] = 1;

        //查看效果
        for (int[] row:map) {
            for (int val:row) {
                System.out.printf("\t"+val);
            }
            System.out.println();
        }

        //小球移动
        isRun(map, 1, 1);
        //查看效果
        System.out.println("-------小球走的路线----------");
        for (int[] row:map) {
            for (int val:row) {
                System.out.printf("\t"+val);
            }
            System.out.println();
        }

    }

    /**
     * 1.小球从哪一个位置开始出发，起始位置 1.1
     * 2.map是地图对象
     * 3.最终小球到达 6.5
     * 4.元素为0时表示该点没有走过，元素为1表示是墙，元素为2时表示通过-可以走，当元素为3时表示已经走过了
     *   -但是走不通
     */
    public static boolean isRun(int[][] map,int i,int j){
        if (map[6][5]==2){  //判断是否到达终点
            return true;
        }else {  //没有达到
            if (map[i][j] == 0){ //是否没走过
                map[i][j] = 2;   //走过

                //行走规则：下、右、上、左
                if (isRun(map,i+1,j)){         //往下走
                    return true;
                }else if (isRun(map, i, j+1)){ //往右走
                    return true;
                }else if (isRun(map, i-1, j)){ //往上走
                    return true;
                }else if (isRun(map, i, j-1)){ //往左走
                    return true;
                }else {
                    map[i][j] = 3;  //走不通就设为3
                }
            }else {
                return false;  //不为0 ，表示不能走了
            }
        }



        return true;

    }
}
