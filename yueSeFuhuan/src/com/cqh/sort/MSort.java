package com.cqh.sort;

import java.util.Arrays;

/**
 *  归并排序
 */
public class MSort {

    public static void main(String[] args) {

        int[] array = new int[]{6,9,4,7,1,2,0,5,3,8};

        //临时数组
        int[] temp = new int[array.length];

        sort(array, 0, array.length-1, temp);

        System.out.println(Arrays.toString(array));


    }

    public static void sort(int[] array,int left,int right,int[] temp){
        //左边小标小于右边
        if (left < right) {
            //求出中间值
            int mid = (left + right)/2;
            /**
             *  向左边分解
             */
            sort(array, left, mid, temp);

            /**
             * 向右边分解
             */
            sort(array, mid+1, right, temp);

            /**
             * 合并数据
             */
            sum(array, left, right, mid, temp);
        }

    }
    public static void sum(int[] array,int left,int right,int mid,int[] temp){

        int i = left;

        int j = mid+1;
        //指向临时数组下标
        int t = 0;

        /**
         * 开始循环比较左右两边数组
         */
        while (i<=mid && j<=right){

            if (array[i] <= array[j]){
                temp[t] = array[i];
                t++;
                i++;
            }else {
                temp[t] = array[j];
                t++;
                j++;
            }
        }
        /**
         * 把剩余得数组放在临时数组中
         */
        while (i <= mid){   //剩余的是 右边数组
            temp[t] = array[i];
            t++;
            i++;
        }
        while (j <= right){   //剩余的是 左边数组
            temp[t] = array[j];
            t++;
            j++;
        }

        /**
         * 临时数组中的元素拷贝到原数组中
         */
        int tempLeft = left;
        int k = 0;
        while (tempLeft <= right){
            array[tempLeft] = temp[k];
            k++;
            tempLeft++;
        }
    }
}
