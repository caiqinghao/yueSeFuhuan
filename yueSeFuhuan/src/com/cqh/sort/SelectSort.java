package com.cqh.sort;

import java.util.Arrays;

/**
 * 选择排序
 */
public class SelectSort {

    public static void main(String[] args) {

        int[] arr = new int[]{3,2,4,5,6,8,7,1};

        //这个循环控制逐步加入数据
        for (int i = 0; i < arr.length; i++) { //从左往右
            //这个循环控制 剩下的数据进行比较
            for (int j = arr.length-1; j > i ; j--) { //右边 到左边还有多少个数据要进行比较
                if (arr[j] < arr[i]){ //顺序
                    int temp = 0;
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));

    }

}
