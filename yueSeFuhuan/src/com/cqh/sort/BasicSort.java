package com.cqh.sort;

import java.util.Arrays;

/**
 *    基数排序
 */
public class BasicSort {

    public static void main(String[] args) {

        int[] arrays = new int[]{53,542,3,63,14,214,154,748,616};
        sort(arrays);

    }

    /**
     * 1.获取原序列的最大位是多少
     */
    public static void sort(int[] arrays){

        /**
         * 获取最大位数，最大值
         */
        int max = 0;
        for (int i = 0; i < arrays.length; i++) {
            if (arrays[i]>max){
                max = arrays[i];
            }
        }
        /**
         * 获取字符串长度，所以把int类型转字符串类型
         */
        int maxLength = ((max+"").length());

        /**
         * 定义一个二维数组，大小10，表示10个桶，每一个桶则是一个数组
         */
        int[][] bucket = new int[10][arrays.length];  // 定义10列，行数是数组个数。

        int[] bucketElementCount = new int[10];   //辅助数组，记录每个桶有多少个数据

        /**
         * 循环获取无序数列
         */
        for (int i = 0; i < arrays.length; i++) {
            int locationElement = arrays[i] % 10 ; //取个位数

            //放入桶中  ,第几个桶，第几个位置。
            bucket[locationElement][bucketElementCount[locationElement]] = arrays[i];
            //辅助数组增加
            bucketElementCount[locationElement]++;
        }
        /**
         * 遍历每一个桶，将元素存放到元素中去
         */
        int index = 0;
        for (int k = 0; k < bucketElementCount.length; k++) {
            if (bucketElementCount[k] != 0){
                for (int l = 0; l < bucketElementCount[k]; l++) {
                    //把数据按桶的顺序放入原数组中
                    arrays[index++] = bucket[k][l];
                }
            }
            //拿走数据后，设为0
            bucketElementCount[k] = 0;
        }
        //打印数组
        System.out.println(Arrays.toString(arrays));

        /**
         * 第一轮对个位数进行比较
         */
        for (int j = 0; j < arrays.length; j++) {
            int locationElement = arrays[j]/1%10;

            bucket[locationElement][bucketElementCount[locationElement]] = arrays[j];
            bucketElementCount[locationElement]++;
        }
        /**
         * 遍历每一个桶，将元素存放到元素中去
         */
        int index01 = 0;
        for (int k = 0; k < bucketElementCount.length; k++) {
            if (bucketElementCount[k] != 0){
                for (int l = 0; l < bucketElementCount[k]; l++) {
                    //把数据按桶的顺序放入原数组中
                    arrays[index01++] = bucket[k][l];
                }
            }
            //拿走数据后，设为0
            bucketElementCount[k] = 0;
        }

        //打印数组
        System.out.println("个位排序后："+Arrays.toString(arrays));
        /**
         * 第二轮，判断十位数
         */
        for (int j = 0; j < arrays.length; j++) {
            int locationElement = arrays[j]/10%10;

            bucket[locationElement][bucketElementCount[locationElement]] = arrays[j];
            bucketElementCount[locationElement]++;
        }
        /**
         * 遍历每一个桶，将元素存放到元素中去
         */
        int index02 = 0;
        for (int k = 0; k < bucketElementCount.length; k++) {
            if (bucketElementCount[k] != 0){
                for (int l = 0; l < bucketElementCount[k]; l++) {
                    //把数据按桶的顺序放入原数组中
                    arrays[index02++] = bucket[k][l];
                }
            }
            //拿走数据后，设为0
            bucketElementCount[k] = 0;
        }
        //打印数组
        System.out.println("十位排序后："+Arrays.toString(arrays));

        /**
         * 第三轮，判断百位数
         */
        for (int j = 0; j < arrays.length; j++) {
            int locationElement = arrays[j]/100%10;

            bucket[locationElement][bucketElementCount[locationElement]] = arrays[j];
            bucketElementCount[locationElement]++;
        }
        /**
         * 遍历每一个桶，将元素存放到元素中去
         */
        int index03 = 0;
        for (int k = 0; k < bucketElementCount.length; k++) {
            if (bucketElementCount[k] != 0){
                for (int l = 0; l < bucketElementCount[k]; l++) {
                    //把数据按桶的顺序放入原数组中
                    arrays[index03++] = bucket[k][l];
                }
            }
            //拿走数据后，设为0
            bucketElementCount[k] = 0;
        }
        //打印数组
        System.out.println("百位排序后："+Arrays.toString(arrays));

    }


}
