package com.cqh.sort;

import java.util.Arrays;

/**
 *  插入排序
 */
public class InsertSort {

    public static void main(String[] args) {

        int[] arrays = new int[]{2,5,6,3,4,7,1,8};
        //控制拿每个元素
        for (int i = 1; i < arrays.length; i++) {
            //比较的次数
            for (int j = i; j >= 1 ; j--) {
                //是否小于前面的数
                if (arrays[j-1] > arrays[j]){
                    int temp = 0;
                    temp = arrays[j];
                    arrays[j] = arrays[j-1];
                    arrays[j-1] = temp;
                }else {
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(arrays));

    }

}
