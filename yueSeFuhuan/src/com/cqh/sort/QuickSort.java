package com.cqh.sort;


import java.util.Arrays;

/**
 *  快速排序
 */
public class QuickSort {

    public static void main(String[] args) {

        int[] arrays = new int[]{2,9,4,7,3,3,6,5};
        sort(arrays, 0, arrays.length-1);
        System.out.println(Arrays.toString(arrays));

    }

    public static void sort(int[] arrays,int left,int right){
        int l = left;
        int r = right;
        int pivot = arrays[(left+right)/2]; //中间值

        while (l<r){
            //在左边查找小于于中间值的
            while (arrays[l] < pivot){
                l++;  //左边往右移动
            }

            //查询右边小于中间值
            while (arrays[r] > pivot){
                r--; //右边往左移动
            }

            if (l >= r){
                break;
            }
            int temp =0;
            temp = arrays[l];
            arrays[l] = arrays[r];
            arrays[r] = temp;

            /**
             *  交换完数据 arrays[l] = pivot
             */
            if (arrays[l] == pivot){
                r--;
            }
            if (arrays[r] == pivot){
                l++;
            }
            if (r == l){
                l++;
                r--;
            }
            if (left<r){ //递归左半边
                sort(arrays,left,r);
            }
            if (right > l){  //递归右半边
                sort(arrays,l,right);
            }
        }

    }

}
