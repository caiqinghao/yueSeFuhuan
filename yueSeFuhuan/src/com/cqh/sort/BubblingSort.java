package com.cqh.sort;


import java.util.Arrays;

/**
 *   冒泡排序
 */
public class BubblingSort {

    public static void main(String[] args) {

        int[] arrays = new int[]{5,4,6,3,2,1};


        for (int j = 0; j < arrays.length-1; j++) {
            for (int i = 0; i < arrays.length-1-j; i++) {
                if (arrays[i] > arrays[i+1]){
                    int temp = 0;
                    temp = arrays[i];
                    arrays[i] = arrays[i+1];
                    arrays[i+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arrays));

    }

}
