package com.cqh.select;

import java.util.Arrays;

/**
 *  斐波那契查找算法(黄金分割算法)
 */
public class FibonacciSelect {

    public static void main(String[] args) {

        int[] array = new int[]{1,1,2,3,5,8,13,21,34,55,89};

        System.out.println(select(array, 13));

    }

    /**
     *  f[k]-1 = (f[k-1]-1) + (f[k-2]-1)
     *  f[k] = (f[k-1]) + (f[k-2])
     */
    public static int[] f(){
        int[] f = new int[20];
        f[0] = 1;
        f[1] = 1;
        for (int i = 2; i < f.length; i++) {
            f[i] = f[i-1] + f[i-2];
        }
        return f;
    }

    /**
     *  mid = low + F(k-1)-1
     */

    public static int select(int[] array,int key){

        int low = 0; //左边
        int hight = array.length-1; //右边
        int k = 0;
        int mid = 0;
        int[] f = f();

        //找分割点
        while (hight > f[k]-1){
            k++;
        }
        int[] temp = Arrays.copyOf(array, f[k]); //进行比较
        // array 进行了扩充
        for (int i = hight+1; i < temp.length; i++) {
            temp[i] = array[hight];  //后面的数都是最后一个数
        }
        while (low <= hight){
            mid = low+f[k-1]-1;
            if (key<temp[mid]){
                //f[k-1]+f[k-2] = f[k];
                hight=mid-1;
                k--;
            }else if (key>temp[mid]){
                low = mid+1;
                k-=2;

            }else {
                if (mid<=hight){
                    return mid;
                }else {
                    return hight;
                }
            }
        }
        return -1;
    }

}
