package com.cqh.select;

/**
 *  插值查找算法
 */
public class InsertSelect {

    public static void main(String[] args) {

        int[] array = new int[]{1,2,3,4,5,6};

        int left = 0;
        int right = array.length-1;

        int searchVal = 3;

        int index = select(array, left, right, searchVal);
        System.out.println(index);
    }

    public static int select(int[] array,int left,int right,int searchVal){

        //防止数组越界
        if (left > right || searchVal < array[0] || searchVal > array[array.length-1]){
            return -1;
        }

        //插值查找 的公式
        int mid = left+(right+left)*(searchVal-array[left])/(array[right]-array[left]);
        int midVal = array[mid];
        if (searchVal > midVal){   //目标值在右边
            return select(array, mid+1, right, searchVal);
        }else if (searchVal < mid){  //目标值在左边
            return select(array,left,mid-1,searchVal);
        }else {    //目标值不存在
            return mid;
        }
    }
}
