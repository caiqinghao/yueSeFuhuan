package com.cqh.select;

/**
 * 二分查找
 */
public class BinarySelect {

    public static void main(String[] args) {

        int[] array = new int[]{10,11,12,13,14,15,16,17};
        int target = 17; //要查询的数

        int index = search(array, target);
        if (index != -1){
            System.out.println("找到目标元素，位置在"+index);
        }else {
            System.out.println("未找到");
        }



    }

    public static int search(int[] array,int target){

        int min = 0;   //最小索引指针
        int max = array.length-1;  //最大索引指针

        while (min <= max){
            //算出平均索引位置
            int mid = (min+max)/2;
            if (array[mid]==target){     //等于目标元素
                return mid;
                //System.out.println("找到目标元素"+mid);
            }

            if (array[mid] < target){
                min = mid+1;
            }

            if (array[mid] > target){
                max = mid-1;
            }
        }
        return -1;
    }

}
