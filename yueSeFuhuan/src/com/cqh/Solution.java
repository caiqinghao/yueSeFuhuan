package com.cqh;

import java.util.*;


public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int money = sc.nextInt();
        int[] num = {100,50,20,10,5,1};//该数组用于存储面值
        int[] count = new int[6];//桶排序用于存储面值所用的个数
        int sum=0;//money是输入的值，sum是目前加起来的钱数

        while (sum != money) {//如果总和不等于money数，就一直循环
            for (int i = 0; i < 6; i++) {//从最大的num[0]即100先开始选取
                if (sum+num[i]  <= money) {
                    //先看i=0即100的需要用几张，如果加100小于等于money，即可以加一张一百，一百面值的个数count[0]加1，然后跳出for循环，进入while循环再从i=0来一遍
                    sum += num[i];
                    count[i]++;
                    break;
                }
                else {//若大于的话，就要i++，一直往后找一个稍小面值的，直到能够进入第一个循环
                    continue;
                }
            }
        }
        for (int i = 0; i < 6; i++) {
            System.out.println("钱："+num[i]+"数量："+count[i]);
        }

    }

}
